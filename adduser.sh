#!/bin/sh

# source ./setting.sh

# ユーザの追加とパスワードの設定
useradd ${HOST}
echo ${USERPASS} | passwd ${HOST} --stdin

# 新規ユーザをwheelグループに追加
gpasswd -a ${HOST} wheel
echo "${HOST}をwheelグループに追加しました"

# 作成したユーザでパスワードによるログインができない場合は
# sshd_configの設定を疑う（↓yesにする）
# PasswordAuthentication no

# ユーザ（名のみ）一覧
# cut -d: -f1 /etc/passwd

# CentOS7では編集不要か？
# visudo
# %wheel ALL=(ALL) ALL #コメントはずす

# wheel グループ以外から su コマンドを実行できなくする
# vim /etc/pam.d/su
# auth required pam_wheel.so use_uid #コメントはずす
cat << EOT >> /etc/pam.d/su
auth required pam_wheel.so use_uid
EOT

# Bitbucket用の秘密鍵を作成
# mkdir ~/.ssh/bitbucket
# パスフレーズもファイル名も確認しない
# ssh-keygen -t rsa -b 4096 -C "${EMAIL}" -P "" -f ~/.ssh/bitbucket/id_rsa
# chmod 600 ~/.ssh/bitbucket/id_rsa
# echo "key was successfully created."

# cat << EOT >> ~/.ssh/config
# Host bitbucket.org
#   HostName bitbucket.org
#   IdentityFile ~/.ssh/bitbucket/id_rsa
#   User git
#   Port 22
#   TCPKeepAlive yes
#   IdentitiesOnly yes
# EOT
# echo "config was successfully created."

# sshd再起動
sudo systemctl restart sshd.service

# cat ~/.ssh/bitbucket/id_rsa.pub
# コピーしてbitbucketへ登録

# 確認
# ssh -T git@bitbucket.org
