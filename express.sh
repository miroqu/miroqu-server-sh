#!/bin/bash

# source ./setting.sh

# install app
cd /home/${HOST}
git init
git remote add origin https://bitbucket.org/miroqu/app.miroqu.com.git
git fetch origin
git merge origin/master
# ブランチがある場合は指定する
# git checkout [branch]

# install modules
npm i --production

# install pm2 if not
if ! rpm -qa | grep -qw pm2; then
  npm install -g pm2@latest
fi

pm2 start ./bin/www

# check out firewalld settings if not access denied.
