#!/bin/bash

# source ./setting.sh

if ! rpm -qa | grep -qw firewalld; then
    yum -y install firewalld
fi
systemctl enable firewalld
systemctl start firewalld

# http https sshを許可
firewall-cmd --permanent --zone=public --add-service=http
firewall-cmd --permanent --zone=public --add-service=https
firewall-cmd --permanent --zone=public --add-service=ssh

# Nodeアプリ用にポート(3000)を開く
firewall-cmd --zone=public --add-port=3000/tcp --permanent

firewall-cmd --reload
systemctl restart sshd.service
