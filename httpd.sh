sh -c "cat << 'EOT' > /etc/httpd/conf.d/${HOST}.conf
<VirtualHost *:80>
  ServerName www.miroqu.com
  DirectoryIndex index.html index.php
  AddDefaultCharset UTF-8
  DocumentRoot /var/www/htmlmiroqu.com/
  ProxyPass / http://localhost:3000/
  # Add a comment to this line
  ProxyPassReverse / http://localhost:3000/
  <Directory "/var/www/html/miroqu.com/">
    AllowOverride All
    Options FollowSymLinks -Indexes
  </Directory>
</VirtualHost>
EOT