#!/bin/sh

set -ux

DIR=$(cd $(dirname $0); pwd)

source ${DIR}/setting.sh

# yum -y update # 時間かかる
yum -y install vim git
cp -b ./files/.vimrc ~/

echo "..."
read Wait

# firewall
source ${DIR}/firewalld.sh

echo "..."
read Wait

# Create new user
# Create secret key pair for Bitbucket
source ${DIR}/adduser.sh

echo "..."
read Wait

# Install Node.js, Nginx
source ${DIR}/nginx.sh
source ${DIR}/node.sh

echo "..."
read Wait

# Install Express and PM2
# express.shの中で階層を移動するため最後に持ってこないと
# それ以下の読み込みができない（やり方があった思うけど忘れた）
source ${DIR}/express.sh

echo "..."
read Wait

# set hostname
source ${DIR}/hostname.sh

echo "..."
read Wait

source ${DIR}/version.sh
source ${DIR}/postfix.sh

# Get more secured
# source ${DIR}/sshd.sh

echo "************************
      ******** DONE! *********
      ************************"
