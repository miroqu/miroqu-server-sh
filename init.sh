#!/bin/sh

source ./setting.sh

# #########################

set -e

read -p "update yum ? (y/n/s)" one
case $one in
    [Yy] | [Yy][Ee][Ss] )
        yum -y update
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "install vim & set .vimrc ? (y/n/s)" two
case $two in
    [Yy] | [Yy][Ee][Ss] )
      if ! rpm -qa | grep -qw vim; then
        yum -y install vim
      fi
      cp -b ./files/.vimrc ~/
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "install git ? (y/n/s)" three
case $three in
    [Yy] | [Yy][Ee][Ss] )
        yum -y install git
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "add user? (y/n/s)" zero
case $zero in
    [Yy] | [Yy][Ee][Ss] )
        source ./adduser.sh
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "edit hosts as ${HOST} ? (y/n/s)" five
case $five in
    [Yy] | [Yy][Ee][Ss] )
        shostnamectl set-hostname ${HOST}
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "install node.js ? (y/n/s)" seven
case $seven in
    [Yy] | [Yy][Ee][Ss] )
        source ./node.sh
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "install nginx ? (y/n/s)" eight
case $eight in
    [Yy] | [Yy][Ee][Ss] )
        source ./nginx.sh
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "enable firewalld for http to work ? (y/n/s)" nine
case $nine in
    [Yy] | [Yy][Ee][Ss] )
        source ./firewalld.sh
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "generate ssh key ? (y/n/s)" ten
case $ten in
    [Yy] | [Yy][Ee][Ss] )
        source ./ssh-keygen.sh
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

read -p "install Express App ? (y/n/s)" eleven
case $eleven in
    [Yy] | [Yy][Ee][Ss] )
        source ./express-demo.sh
        ;;
    [Ss] | s )
        echo "Skipped!";;
    * )
        echo "Quit"; exit 1;;
esac

echo "
*************************************
**************** Done! **************
*************************************"
