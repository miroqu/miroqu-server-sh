#!/bin/sh

yum -y update
yum install epel-release
rpm -Uvh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

# php
yum -y install --enablerepo=remi,remi-php56 php php-common php-opcache php-devel php-mbstring php-mcrypt php-mysqlnd php-phpunit-PHPUnit php-pecl-xdebug php-pecl-xhprof
yum install --enablerepo=remi,remi-php56 -y php-intl

# apache
yum install --enablerepo=remi,remi-php56 -y httpd
systemctl start httpd

# mysql
yum remove mariadb-libs
rm -rf /var/lib/mysql/
wget http://dev.mysql.com/get/Downloads/MySQL-5.6/MySQL-5.6.25-1.el7.x86_64.rpm-bundle.tar
rpm -Uvh mysql-community-release-el7-5.noarch.rpm
yum -y install mysql-community-server
systemctl enable mysqld.service

cat <<EOT >> vim /etc/my.cnf
character_set_server=utf8
skip-character-set-client-handshake
EOT
