#!/bin/bash

source ./setting.sh

# if old version was installed
# yum remove nginx
sh -c "cat << 'EOT' > /etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/
gpgcheck=0
enabled=1
EOT"

if ! rpm -qa | grep -qw nginx; then
  yum -y --enablerepo=nginx install nginx
  systemctl start nginx
  systemctl enable nginx
else
  echo "Nginx is already installed."
fi

# Create directories & log files
# mkdir /home/${HOST} # the user folder has been created when user added.
# mkdir /home/${HOST}/logs
# touch /home/${HOST}/logs/access.log
# touch /home/${HOST}/logs/error.log

# add /etc/nginx/conf.d/*.conf
sh -c "cat << 'EOT' > /etc/nginx/conf.d/${HOST}.conf
upstream node-app {
    server ${HOST}:3000;
}
server {
    listen       80;
    server_name  www.${HOST};
    return       301 http://${HOST}\$request_uri;
}
server {
    listen 80;
	  listen [::]:80;
    server_name  ${HOST};
    proxy_redirect                          off;
    proxy_set_header Host                   \$host;
    proxy_set_header X-Real-IP              \$remote_addr;
    proxy_set_header X-Forwarded-Host       \$host;
    proxy_set_header X-Forwarded-Server     \$host;
    proxy_set_header X-Forwarded-For        \$proxy_add_x_forwarded_for;
    location / {
        proxy_pass http://node-app/;
    }
    error_page   500 502 503 504  /50x.html;
    location = /50x.html {
    	root /home/${HOST};
    }
}
EOT"

systemctl restart nginx
