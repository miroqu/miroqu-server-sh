#!/bin/bash

source ./version.sh

sh -c "cat << 'EOT' > /usr/share/nginx/html/status.html
<!DOCTYPE html>
<html lang=\"en\">
<head>
  <meta charset=\"UTF-8\">
  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">
  <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">
  <style>
    html {
      font-family: Consolas,\"Liberation Mono\",Menlo,Courier,monospace
    }
  </style>
  <title>Status</title>
</head>
<body>
  <ul>
  <li>${HOST}</li>
  <li>OS : ${VAR1}</li>
  <li>Node.js : ${VAR2}</li>
  <li>${VAR3}</li>
  <li>Firewalld : ${VAR4}</li>
  <li>${VAR5}</li>
  <li>${VAR6}</li>
  <li>${VAR7}</li>
</body>
</html>
EOT"
