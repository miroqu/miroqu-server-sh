#!/bin/sh

# cyrus-saslのインストール
yum -y install cyrus-sasl
systemctl start saslauthd
systemctl enable saslauthd

# saslauthdからauxpropに変更
cat << EOT >> /etc/sasl2/smtpd.conf
pwcheck_method: auxprop
mech_list: plain login
EOT

# Postfixをが無ければインストール
if ! rpm -qa | grep -qw postfix; then
  yum -y install postfix
fi

# 設定ファイルの編集
cat << EOT >> /etc/postfix/main.cf
myhostname = mail.${HOST}
mydomain = ${HOST}
myorigin = \$mydomain
mydestination = myhostname,localhost.myhostname,localhost.mydomain, localhost, \$mydomain
home_mailbox = Maildir/
smtpd_sasl_auth_enable = yes
smtpd_recipient_restrictions =
    permit_mynetworks
    permit_sasl_authenticated
    reject_unauth_destination
EOT

systemctl restart postfix
systemctl enable postfix

# テストメール送信
cat << EOM | mail -s "インストールが完了しました！" -r admin ${EMAIL}
すべてのスクリプトが実行され、インストールが完了しました。
以下は各プログラムのバージョン情報です。

${CENTOS}
${NODEJS}
${NGINX}
${FIREWALLD}
${GIT}
${EXPRESS}
${HOSTNAME}

+----------+--------------------------------------+
| os       | ${CENTOS} |
+----------+--------------------------------------+
| node.js  | ${NODEJS}                               |
+----------+--------------------------------------+
| nginx    | ${NGINX}          |
+----------+--------------------------------------+
| hostname | ${HOSTNAME}                      |
+----------+--------------------------------------+
EOM
