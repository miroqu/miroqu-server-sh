#!/bin/sh

curl -O http://repository.rainloop.net/v2/webmail/rainloop-latest.zip
mkdir /var/www/html/rainloop
unzip rainloop-latest.zip -d /var/www/html/rainloop
find /var/www/html/rainloop -type d -exec chmod 755 {} \;
find /var/www/html/rainloop -type f -exec chmod 644 {} \;
chown -R apache. /var/www/html/rainloop
