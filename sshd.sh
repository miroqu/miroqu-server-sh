#!/bin/sh

# source ./setting.sh

# すでに作成済みの場合はssh-keygenは不要
# ssh-keygen -t rsa -b 4096 -C "${EMAIL}" -P "" -f id_rsa
# ${HOST}と新規ユーザ名は同じ値
echo "scp ~/.ssh/id_rsa.pub ${HOST}@${IPADRESS}:~/.ssh/authorized_keys"
echo "上記コマンドで秘密鍵をホストへコピーしてください"
echo "Press ENTER to continue..."
read Wait

# サーバ側で
chmod 0600 ~/.ssh/authorized_keys
chown ${HOST}:${HOST} ~/.ssh/authorized_keys

# sshd_configの編集(追記)
cat << EOT >> /etc/ssh/sshd_config
Port ${PORT}
RSAAuthentication yes
PubkeyAuthentication yes
AuthorizedKeysFile .ssh/authorized_keys
PermitRootLogin no
PasswordAuthentication no
EOT
echo "sshd_config was successfully edited."

# firewalldでSSHポート番号を変更
# ポート番号を置換えて指定の場所に保存
sed -e "s/port=\"22\"/port=\"${PORT}\"/" /usr/lib/firewalld/services/ssh.xml > /etc/firewalld/services/ssh.xml

# 再起動
systemctl restart sshd.service

# 確認
# systemctl list-unit-files -t service

# 個別確認
# systemctl status sshd.service

# 自動起動
systemctl enable sshd.service

# 再読込
firewall-cmd --reload
