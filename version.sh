#!/bin/bash

CENTOS=`cat /etc/redhat-release`
NODEJS=`node -v`
NGINX=`nginx -v 2>&1`
FIREWALLD=`firewall-cmd --list-services --zone=public --permanent`
GIT=`git --version`
EXPRESS=`express --version`
HOSTNAME=`hostname`
