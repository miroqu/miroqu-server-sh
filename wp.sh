#!/bin/sh

DBNAME="miroqu_wp"
DBUSER="miroqu"
DBPASS="miroqu"
DBHOST="localhost"
DBPREFIX="wp_"

# ############

SITE_URL="http://miroqu.local/wordpress/"
SITE_TITLE="Wordpress"
ADMIN_USER="admin"
ADMIN_PASSWORD="admin"
ADMIN_EMAIL="km@oflite.com"

# List all plugins into array
# PLUGINS=(
# 	"rest-api",
# 	"wp-rest-api-v2-menus",
# 	"wp-multibyte-patch"
# 	# "https://bitbucket.org/miroqu/miroqu-wp-plugin/miroqu-wp-plugin.zip"
# 	)

# ############

# Download Wordpress
wp core download --locale=ja --path=/home/miroqu.local/wordpress

# SetUp wp-config
wp core config --dbname=${DBNAME} --dbuser=${DBUSER} --dbpass=${DBPASS} --dbhost=${DBHOST} --dbprefix=${DBPREFIX} --path=/home/miroqu.local/wordpress

# Wordpress Install
wp core install --url=${SITE_URL} --title=${SITE_TITLE} --admin_user=${ADMIN_USER} --admin_password=${ADMIN_PASSWORD} --admin_email=${ADMIN_EMAIL} --path=/home/miroqu.local/wordpress

# Plugins Download & Install
# for plugin in ${PLUGINS[@]}
# do
#   wp plugin install $plugin --activate --path=/home/miroqu.local/wordpress
# done

wp plugin install rest-api --activate --path=/home/miroqu.local/wordpress
wp plugin install wp-rest-api-v2-menus --activate --path=/home/miroqu.local/wordpress
wp plugin install wp-multibyte-patch --activate --path=/home/miroqu.local/wordpress
